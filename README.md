# OPUS4 Docker

## Init

Clone the opus4 application
```bash
git clone https://github.com/OPUS4/application opus/application
```

Clone the opus4 search
```bash
git clone https://github.com/OPUS4/opus4-search
```

## Usage

Initially, the custom containers need to be built with
```bash
docker-compose build
```
and
```bash
docker-compose -f docker-compose.initdb.yml build
```


To initialize the database, run
```bash
docker-compose -f docker-compose.initdb.yml up --abort-on-container-failure
```
This needs to be stopped manually with `CTRL+C`
when service `init_tables` has exited.

> Note: Be careful with repeating the previous step - this will drop the database!

Now that the database container and the opus tables have been initialized,
you can bring the containers up with
```bash
docker-compose up --abort-on-container-failure
```

### OPUS4

OPUS4 is now available in your browser at [http://localhost/](http://localhost/).

The admin account is currently hardcoded in `application/db/masterdata/004_create_user_accounts.sql`
to user `admin` with password `adminadmin`.

### Adminer

An instance of [adminer](https://www.adminer.org/en/)
is availble at [http://localhost:8080](http://localhost:8080).

Log in there as `root` with `$MYSQL_ROOT_PASSWORD` given in [.env](.env).

### Solr

An instance of [Solr](https://solr.apache.org/) admin
is available at [http://localhost:8983](http://localhost:8983).